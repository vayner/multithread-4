/*
 *  IMT3801 Multi-threaded Programming - Task 4.2
 *
 *   121240 - 12HBSPA
 *   Tellef Møllerup Åmdal
 */

#include <stdlib.h>
#include <omp.h>

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

int stage = 0;
long long value = 0;
bool quit = false;

std::condition_variable cvReader;
std::condition_variable cvMultiplier;
std::condition_variable cvPrinter;

std::mutex mValue;

/**
 * @fn  void ValueReader();
 *
 * @brief   Value reader thread.
 */

void ValueReader();

/**
 * @fn  void ValueDoubler();
 *
 * @brief   Value doubling thread.
 */

void ValueDoubler();

/**
 * @fn  void ValuePrinter();
 *
 * @brief   Value printer thread.
 */

void ValuePrinter();

/**
 * @fn  int main()
 *
 * @brief   Main entry-point for this application.
 *
 * @return  Exit-code for the process - 0 for success, else an error code.
 */

int main() {

    std::thread input(ValueReader);
    std::thread doubler(ValueDoubler);
    std::thread printer(ValuePrinter);

    input.join();
    doubler.join();
    printer.join();

    return 0;
}


void ValueReader() {
    std::string input;

    while (true) {
        std::unique_lock<std::mutex> lock(mValue);
        cvReader.wait(lock, [] {return stage == 0; });

        std::getline(std::cin, input);

        if(input.length() == 0) {
            printf("You got to write something, stop spamming enter\n");
            continue;
        }

        if (input.at(0) == 'q') {
            break;
        }

        try {
            value = std::stoll(input);
        } catch (std::invalid_argument e) {
            printf("Not a number, ignoring\n");
            continue;
        } catch (std::out_of_range e) {
            printf("Too large a number, ignoring\n");
            continue;
        }

        printf("Sending: %lld\n", value);

        stage = 1;
        lock.unlock();
        cvMultiplier.notify_one();
    }

    printf("ValueReader() quiting\n");

    quit = true;
    cvMultiplier.notify_one();
}

void ValueDoubler() {

    while (true) {
        std::unique_lock<std::mutex> lock(mValue);
        cvMultiplier.wait(lock, [] {return stage == 1 || quit; });
        if (quit) {
            break;
        }
        
        printf("Turned %lld into %lld\n", value, value * 2);

        long long runs = value;
#pragma omp parallel for
        for(int i = 0; i < runs; i++) {
#pragma omp atomic
			++value;        	
        }

        stage = 2;
        lock.unlock();
        cvPrinter.notify_one();
    }

    printf("ValueDoubler() quiting\n");

    cvPrinter.notify_one();
}

void ValuePrinter() {

    while (true) {
        std::unique_lock<std::mutex> lock(mValue);
        cvPrinter.wait(lock, [] {return stage == 2 || quit; });
        if (quit) {
            break;
        }

        printf("Printing %lld\n", value);

        stage = 0;
        lock.unlock();
        cvReader.notify_one();
    }

    printf("ValuePrinter() quiting\n");
}