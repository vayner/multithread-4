/*
 *  IMT3801 Multi-threaded Programming - Task 4.1
 *
 *   121240 - 12HBSPA
 *   Tellef Møllerup Åmdal
 */

#include <stdio.h>

#include <string>
#include <thread>

#include "SafeQueue.h"

const int NUM_THREADS = 100;
const int ITERATIONS = 1000;

SafeQueue<std::string> stackA;
SafeQueue<std::string*> stackB;


void pushThread() {

    for (int i = 0; i < ITERATIONS; ++i) {
        stackA.push("HAHA");
        stackA.pop();
        stackA.push("LALALA");
    }
}

void popThread() {

    for (int i = 0; i < ITERATIONS; ++i) {
        std::string* front = stackB.front();
        stackB.pop();
        printf("pop: %s\n", front->c_str());
    }
}

int main () {

    std::array<std::thread, NUM_THREADS> threads;

    for (auto &t : threads) {
        t = std::thread(pushThread);
    }

    for (auto &t : threads) {
        t.join();
    }

    printf("%lu == %i  %s\n", stackA.size(), ITERATIONS * NUM_THREADS, stackA.front().c_str());

    for (int i = 0; i < ITERATIONS * NUM_THREADS; ++i) {
        stackB.push(new std::string(std::to_string(i)));
    }

    for (auto &t : threads) {
        t = std::thread(popThread);
    }

    for (auto &t : threads) {
        t.join();
    }

    return 0;
}