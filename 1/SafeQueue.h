#pragma once

#include <queue>
#include <mutex>

template<typename T>
class SafeQueue {
private:
    std::queue<T> data;
    std::mutex* mutex;

public:

    SafeQueue(){
        mutex = new std::mutex();
    };

    ~SafeQueue() {
        delete mutex;
    };

    T& front() {
        std::lock_guard<std::mutex> guard(*mutex);
        return data.front();
    };

    const T& front() const {
        std::lock_guard<std::mutex> guard(*mutex);
        return data.front();
    };

    bool empty() const{
        std::lock_guard<std::mutex> guard(*mutex);
        return data.empty();
    }

    size_t size() const {
        std::lock_guard<std::mutex> guard(*mutex);
        return data.size();
    }

    void push(const T& value) {
        std::lock_guard<std::mutex> guard(*mutex);
        data.push(value);
    }

    void push(T&& value) {
        std::lock_guard<std::mutex> guard(*mutex);
        data.push(value);
    }

    void pop() {
        std::lock_guard<std::mutex> guard(*mutex);
        data.pop();
    }
};