/*
 *  IMT3801 Multi-threaded Programming - Task 4.3
 *
 *   121240 - 12HBSPA
 *   Tellef Møllerup Åmdal
 */

#include <stdio.h>

#include <array>
#include <thread>
#include <future>
#include <queue>

const int NUM_CONSUMERS = 16;
const int NUM_RANDOM_NUMBERS = 255;

const unsigned int RANDOM_SEED = 256956982;

std::queue<int> dataQueue;
std::mutex queueMutex;
std::condition_variable queueCond;
std::atomic_bool controlBit(false);

long long consumeAdd() {

    long long sum = 0LL;
    int value;

    while(true) {
        std::unique_lock<std::mutex> guard(queueMutex);
        if(dataQueue.size() > 0) {
            value = dataQueue.front();
            dataQueue.pop();
            guard.unlock();

            sum += value;
        } else {
            queueCond.wait(guard, [] {return controlBit || dataQueue.size() > 0;});

            if(controlBit && dataQueue.size() == 0) {
                break;
            }
        }
    }

    return sum;
}

int main() {
    srand(RANDOM_SEED);

    std::vector<std::future<long long>> consumerResults;

    for(int i = 0; i < NUM_CONSUMERS; ++i) {
        consumerResults.push_back(std::async(std::launch::async, consumeAdd));
    }

    for (int i = 0; i < NUM_RANDOM_NUMBERS; ++i) {
        std::unique_lock<std::mutex> guard(queueMutex);
        dataQueue.push(rand());
        guard.unlock();
        queueCond.notify_one();
    }

    controlBit = true;
    queueCond.notify_all();

    long long sum = 0LL;

    for (auto &f : consumerResults) {
        sum += f.get();
    }

    printf("Sum:    %lli\n", sum);

    srand(RANDOM_SEED);
    long long targetSum = 0;

    for (int i = 0; i < NUM_RANDOM_NUMBERS; ++i) {
        targetSum += rand();
    }

    printf("Target: %lli\n", targetSum);

    return 0;
}